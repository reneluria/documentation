# Architecture

## Overview

#### Vocabulary

**Fediverse**: several servers following one another, several users
following each other. Designates federated communities in general.

**Vidiverse**: same as Fediverse, but federating videos specifically.

**Instance**: a server which runs PeerTube in the fediverse.

**Origin instance**: the instance on which the video was uploaded and which
is seeding (through the WebSeed protocol) the video.

**Cache instance**: an instance that decided to make available a WebSeed
of its own for a video originating from another instance. It sends a [`CacheFile`
activity](api-activitypub)
to notify the origin instance, which will then update its list of
WebSeeds for the video.

**Following**: the action of a PeerTube instance which will follow another
instance (subscribe to its videos). You can read more about Follows in the admin doc,
under [following servers](admin-following-instances).

#### What is an instance and how does it work

- An instance has a websocket tracker which is responsible for all videos
  uploaded by its users.
- An instance has an administrator that can follow other instances.
- An instance can be configured to follow back automatically.
- An instance can blacklist other instances (only used in "follow back"
  mode).
- An instance cannot choose which other instances follow it, but it can
  decide to **reject all** followers.
- After having uploaded a video, the instance seeds it (WebSeed protocol).
- If a user wants to watch a video, they ask its instance the magnet URI and
  the frontend adds the torrent (with WebTorrent), creates the HTML5 video
  player and streams the file into it.
- A user watching a video seeds it too (BitTorrent). Thus, another user who is
  watching the same video can get the data from the origin server and other
  users watching it.

#### Communications between instances

![Sharing videos metadata to build an index of more than just local videos](/assets/schema/decentralized.png)

- All the communications between the instances are signed with [JSON Linked Data
  Signatures](https://w3c-dvcg.github.io/ld-signatures/) with the private key
  of the account that authored the action.
- We use the [ActivityPub](https://www.w3.org/TR/activitypub/) protocol (only
  server-server for now) as the high-level federation protocol. Object models can be found in
  [shared/models/activitypub
  directory](https://github.com/Chocobozzz/PeerTube/tree/develop/shared/models/activitypub).
- [ActivityStreams](https://www.w3.org/TR/activitystreams-core/) and
  [ActivityStreams](https://www.w3.org/TR/activitystreams-vocabulary/) vocabulary
  as the mean to structure messages.
- All the requests are retried several times if they fail.
- Actor requests are authenticated via [HTTP Signatures](https://datatracker.ietf.org/doc/draft-cavage-http-signatures/) as the secondary mean
  to authenticate messages to keep compatible security-wise with textual instances
  like Mastodon which rely on it.

#### Redundancy between instances

A PeerTube instance can cache other PeerTube videos to improve bandwidth of popular videos
or small instances.

##### How it works

The instance administrator can choose between multiple redundancy strategies (cache trending videos or recently uploaded videos etc), set their maximum size and the minimum duplication lifetime.
Then, they choose the instances they want to cache in `Manage follows -> Following` admin table.

Videos are kept in the cache for at least `min_lifetime`, and then evicted when the cache is full.

When PeerTube chooses a video to duplicate, it imports all the resolution files (to avoid consistency issues) using their magnet URI and put them in the `storage.videos` directory.
Then it sends a `Create -> CacheFile` ActivityPub message to other federated instances. This new instance is injected as [WebSeed](https://github.com/Chocobozzz/PeerTube/blob/develop/FAQ.md#what-is-webseed) in the magnet URI by instances that received this ActivityPub message.

![Cache servers](/assets/schema/redundancy.png)

##### Stats

See the `/api/v1/server/stats` endpoint. For example:

```json
{
  ...
  "videosRedundancy": [
    {
      "totalUsed": 0,
      "totalVideos": 0,
      "totalVideoFiles": 0,
      "strategy": "most-views",
      "totalSize": 104857600
    },
    {
      "totalUsed": 0,
      "totalVideos": 0,
      "totalVideoFiles": 0,
      "strategy": "trending",
      "totalSize": 104857600
    },
    {
      "totalUsed": 0,
      "totalVideos": 0,
      "totalVideoFiles": 0,
      "strategy": "recently-added",
      "totalSize": 104857600
    }
  ]
}
```

## Technical Overview

PeerTube is more than just a web page. Simplifying things a lot, we can
see it as two parts: a client application that executes in the browser of
each visitor (and that can be replaced entirely with a new client of your
choice, of course), and a server part that resides on the machine of the
instance's system administrator.

As is common among modern applications, it is in reality made of several
more components than just a "client" and "server", each of them fulfilling
a specific mission:

- a modern database engine (PostgreSQL) to store long-term metadata
- a reverse proxy (we officially support Nginx but nothing prevents using others)
  to handle certificates, and directly serve static assets
- a key-value store (Redis) to help application caching and task queueing
- a REST API server providing the actual logic, data streaming and embedding an in-memory BitTorrent tracker
- a web client _Single Page Application_ that consumes the REST API
- a user that interacts with the web client of his choice

![architecture overview](/assets/architecture-overview.jpg)

Let's detail each component:

#### The user

PeerTube users can interact with your instance using:

- The official web interface
- Third-party apps (other clients using the REST API)

#### The web interface

This refers to PeerTube's official web interface, which is a Single Page application
written in Angular. This application will interact with PeerTube's API to retrieve
or send data. Of course any alternative client interface can be used so long as it is
compatible with the API, and PeerTube can be started without the official web interface
to let you serve your client instead.

#### The reverse proxy

PeerTube's API server should never be exposed directly to the internet, as we require
a reverse proxy (we provide support for Nginx) for performance and security. The reverse proxy
will receive client HTTP requests, and:

- Proxy them to the API server
- Serve requested static files (Video files, stylesheets, javascript, fonts...)

#### The REST API server

The API server is the central piece of PeerTube. This component is responsible
for answering and processing user requests, manipulate data from the database, send long-running
tasks to the local worker, etc.

It's an [Express](https://expressjs.com/) application.

#### The database

Most of the data such as user accounts, video metadata, comments or channels are stored
in a PostgreSQL database.

#### The cache/job queue

Fetching data from the database is sometimes slow or resource hungry. To reduce
the load, Redis is used as a cache for route data meant to be stored temporarily.

It is also a message queue that will deliver tasks to the local worker. Indeed, PeerTube
uses the [Bull queue](https://github.com/OptimalBits/bull) which doesn't support remote
workers yet.

## Server code

The server is a web server developed with [TypeScript](https://www.typescriptlang.org/)/[Express](http://expressjs.com).


#### Technologies

  * [TypeScript](https://www.typescriptlang.org/) -> Language
  * [PostgreSQL](https://www.postgresql.org/) -> Database
  * [Redis](https://redis.io/) -> Job queue/cache
  * [Express](http://expressjs.com) -> Web server framework
  * [Sequelize](http://docs.sequelizejs.com/en/v3/) -> SQL ORM
  * [WebTorrent](https://webtorrent.io/) -> BitTorrent tracker and torrent creation
  * [Mocha](https://mochajs.org/) -> Test framework


#### Files

The server main file is [server.ts](https://github.com/Chocobozzz/PeerTube/blob/develop/server.ts).
The server modules description are in the [package.json](https://github.com/Chocobozzz/PeerTube/blob/develop/package.json) at the project root.
All other server files are in the [server](https://github.com/Chocobozzz/PeerTube/tree/develop/server) directory:

    server.ts -> app initialization, main routes configuration (static routes...)
    config    -> server YAML configurations (for tests, production...)
    scripts   -> Scripts files for npm run
    server
    |__ controllers  -> API routes/controllers files
    |__ helpers      -> functions used by different part of the project (logger, utils...)
    |__ initializers -> functions used at the server startup (installer, database, constants...)
    |__ lib          -> library function (WebTorrent, OAuth2, ActivityPub...)
    |__ middlewares  -> middlewares for controllers (requests validators, requests pagination...)
    |__ models       -> Sequelize models for each SQL tables (videos, users, accounts...)
    |__ tests        -> API tests and real world simulations (to test the decentralized feature...)


#### Conventions

Uses [JavaScript Standard Style](http://standardjs.com/).

#### Architecture

The server is composed by:

  * a REST API (relying on the Express framework) documented on https://docs.joinpeertube.org/api-rest-reference.html
  * a WebTorrent Tracker (slightly custom version of [webtorrent/bittorrent-tracker](https://github.com/webtorrent/bittorrent-tracker#server))

A video is seeded by the server with the [WebSeed](http://www.bittorrent.org/beps/bep_0019.html) protocol (HTTP).

![Architecture scheme](/assets/schema/architecture-server.png)

When a user uploads a video, the REST API creates the torrent file and then adds it to its database.

If a user wants to watch the video, the tracker will indicate all other users that are watching the video + the HTTP url for the WebSeed.

#### Newcomers

The server entry point is [server.ts](https://github.com/Chocobozzz/PeerTube/blob/develop/server.ts). Looking at this file is a good start.
Then you can try to understand the [controllers](https://github.com/Chocobozzz/PeerTube/tree/develop/server/controllers): they are the entrypoints of each API request.


## Client code

The client is a HTML/CSS/JavaScript web application (single page application -> SPA) developed with [TypeScript](https://www.typescriptlang.org/)/[Angular](https://angular.io/).


#### Technologies

  * [TypeScript](https://www.typescriptlang.org/) -> Language
  * [Angular](https://angular.io) -> JavaScript framework
  * [SASS](http://sass-lang.com/) -> CSS framework
  * [Webpack](https://webpack.js.org/) -> Source builder (compile TypeScript, SASS files, bundle them...)
  * [Bootstrap](http://getbootstrap.com/) -> CSS framework
  * [WebTorrent](https://webtorrent.io/) -> JavaScript library to make P2P in the browser
  * [VideoJS](http://videojs.com/) -> JavaScript player framework


#### Files

The client files are in the `client` directory. The Webpack 2 configurations files are in `client/config` and the source files in `client/src`.
The client modules description are in the [client/package.json](https://github.com/Chocobozzz/PeerTube/tree/develop/client/package.json). There are many modules that are used to compile the web application in development or production mode.
Here is the description of the useful `client` files directory:

    tslint.json   -> TypeScript linter rules
    tsconfig.json -> TypeScript configuration for the compilation
    .bootstraprc  -> Bootstrap configuration file (which module we need)
    config        -> Webpack configuration files
    src
    |__ app          -> TypeScript files for Angular application
    |__ assets       -> static files (images...)
    |__ sass         -> SASS files that are global for the application
    |__ standalone   -> files outside the Angular application (embed HTML page...)
    |__ index.html   -> root HTML file for our Angular application
    |__ main.ts      -> Main TypeScript file that boostraps our Angular application
    |__ polyfills.ts -> Polyfills imports (ES 2015...)

Details of the Angular application file structure. It tries to follow [the official Angular styleguide](https://angular.io/docs/ts/latest/guide/style-guide.html).

    app
    |__ +admin                       -> Admin components (followers, users...)
    |__ account                      -> Account components (password change...)
    |__ core                         -> Core components/services
    |__ header                       -> Header components (logo, search...)
    |__ login                        -> Login component
    |__ menu                         -> Menu component (on the left)
    |__ shared                       -> Shared components/services (search component, REST services...)
    |__ signup                       -> Signup form
    |__ videos                       -> Video components (list, watch, upload...)
    |__ app.component.{html,scss,ts} -> Main application component
    |__ app-routing.module.ts        -> Main Angular routes
    |__ app.module.ts                -> Angular root module that imports all submodules we need

#### Conventions

Uses [TSLint](https://palantir.github.io/tslint/) for TypeScript linting and [Angular styleguide](https://angular.io/docs/ts/latest/guide/style-guide.html).

#### Concepts

In a Angular application, we create components that we put together. Each component is defined by an HTML structure, a TypeScript file and optionally a SASS file.
If you are not familiar with Angular I recommend you to read the [quickstart guide](https://angular.io/docs/ts/latest/quickstart.html).

#### Components tree

![Components tree](/assets/schema/client-components-tree.svg)

#### Newcomers

The main client component is `app.component.ts`. You can begin to look at this file. Then you could navigate in the different submodules to see how components are built.
