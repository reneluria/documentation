# Download a video

You can download videos directly from the web interface of PeerTube, by clicking the dot button "..." aside the title, and then the "Download" option. A choice is then given to you regarding how you want to download:

![Modal presenting options to download a video](/assets/video-share-download.png)

* "Direct Download", which does what it says: your web browser downloads the video from the origin server of the video.
* peer-to-peer download via "Torrent", where you need a WebTorrent compatible client to download the video not only from the origin server but also from other peers watching the video or sharing it from their own WebTorrent-compatible clients at home! (by doing so you help the network be more resilient!) - any BitTorrent can also download the video, but without WebTorrent support they won't be able to exchange with web browsers.

**Tip:** depending on the instance, you can download the video in different formats. However, please make sure you are granted a licence compatible with the planned usage of the video beforehand.
